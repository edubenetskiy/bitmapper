SOURCE_FILES = main.c pixel.h image.c image.h bmp/bmp_header.h point.h bmp/parser.c bmp/parser.h transformations/rotate.c transformations/rotate.h transformations/gaussian_blur.c transformations/gaussian_blur.h transformations/dilate.c transformations/dilate.h transformations/erode.c transformations/erode.h
OUTPUT_FILE = bitmapper

${OUTPUT_FILE}: ${SOURCE_FILES}
	gcc -o ${OUTPUT_FILE} -lm -O3 -pedantic -Wall -Wextra -Winline ${SOURCE_FILES}

clean:
	rm ${OUTPUT_FILE}

.PHONY: clean
