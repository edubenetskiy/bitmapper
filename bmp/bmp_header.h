#ifndef YASP_LAB5_BMP_H
#define YASP_LAB5_BMP_H

#include <stdint.h>

static const uint16_t BMP_FORMAT_SIGNATURE = 0x4D42;
static const uint32_t BITMAP_INFO_HEADER_SIZE = 40;
static const uint32_t BITMAP_V4_HEADER_SIZE = 108;
static const uint32_t BITMAP_V5_HEADER_SIZE = 124;

struct __attribute__((packed, aligned(2)))
bmp_header_s {
    // Signature of BMP format = 0x424D (ASCII ‘BM’).
    uint16_t bfType;

    // File size, bytes.
    uint32_t bfFileSize;

    // Reserved bytes, must equal to zero.
    uint32_t bfReserved;

    // Offset of pixel array in file, bytes from beginning.
    uint32_t bfOffBits;

    // Size of bitmap information header, bytes.
    uint32_t biHeaderSize;

    // Image width, pixels.
    uint32_t biWidth;
    // Image height, pixels.
    uint32_t biHeight;

    // Number of image layers, must equal to 1 for most files.
    uint16_t biPlanes;

    // Color depth, bits per pixel.
    uint16_t biBitCount;

    // Type of compression.
    // Zero = uncompressed two-dimensional array.
    uint32_t biCompression;

    // Number of image bytes after decompression.
    uint32_t biSizeImage;

    // Pixels per meter along X axis.
    uint32_t biXPelsPerMeter;
    // Pixels per meter along Y axis.
    uint32_t biYPelsPerMeter;

    // Offset of the color palette in file, bytes.
    // Usually is not present and equals to zero.
    uint32_t biClrUsed;

    // Number of color palette elements being used.
    uint32_t biClrImportant;
};

typedef struct bmp_header_s bmp_header_t;

#endif //YASP_LAB5_BMP_H
