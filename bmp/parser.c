#include <malloc.h>
#include "parser.h"
#include "bmp_header.h"

uint16_t BITS_PER_PIXEL = sizeof(pixel_t) * 8; // 24

read_status_t read_bmp_header(FILE * in, bmp_header_t * const header)
{
    if (fread(header, sizeof(bmp_header_t), 1, in) != 1)
        return READ_COULD_NOT_READ_HEADER;

    if (header->bfType != BMP_FORMAT_SIGNATURE)
        return READ_INVALID_SIGNATURE;

    if (header->biHeaderSize != BITMAP_INFO_HEADER_SIZE &&
        header->biHeaderSize != BITMAP_V4_HEADER_SIZE &&
        header->biHeaderSize != BITMAP_V5_HEADER_SIZE) {
        return READ_UNSUPPORTED_HEADER_TYPE;
    }

    // TODO: Test if width and height are non-zero

    if (header->biBitCount != BITS_PER_PIXEL)
        return READ_UNSUPPORTED_BITS_PER_PIXEL;

    return READ_OK;
}

read_status_t read_bmp(FILE * in, image_t * const image)
{
    bmp_header_t header;
    read_status_t read_status = read_bmp_header(in, &header);
    if (read_status != READ_OK) {
        return read_status;
    }

    image->width = header.biWidth;
    image->height = header.biHeight;
    image->pixels = calloc(image->width * image->height, sizeof(pixel_t));

    // REVIEW: Read in memory and don't use fseek
    if (fseek(in, header.bfOffBits, SEEK_SET) != 0) {
        free(image->pixels);
        return READ_COULD_NOT_READ_PIXELS;
    }

    // In BMP, pixel rows are padded to the nearest 4 bytes.
    const size_t padding = (4 - (image->width * sizeof(pixel_t)) % 4) % 4;

    pixel_t * pixels = image->pixels + image->height * image->width;

    for (uint32_t i = 0; i < image->height; ++i) {
        pixels -= image->width;
        if (fread(pixels, sizeof(pixel_t), image->width, in) != image->width) {
            free(image->pixels);
            return READ_COULD_NOT_READ_PIXELS;
        }
        fseek(in, padding, SEEK_CUR);
    }
    return READ_OK;
}

write_status_t write_bmp(FILE * out, image_t * const image)
{
    bmp_header_t header = {
            bfType: BMP_FORMAT_SIGNATURE,
            bfOffBits: sizeof(bmp_header_t),
            biWidth: image->width,
            biHeight: image->height,
            biHeaderSize: BITMAP_INFO_HEADER_SIZE,
            biBitCount: BITS_PER_PIXEL,
            biClrUsed: 0,
            biClrImportant: 0,
            biCompression: 0,
    };
    fwrite(&header, sizeof(bmp_header_t), 1, out);

    // In BMP, pixel rows are padded to the nearest 4 bytes.
    const size_t padding = (4 - (image->width * sizeof(pixel_t)) % 4) % 4;

    pixel_t * pixels = image->pixels + image->width * image->height;
    for (uint32_t y = 0; y < image->height; ++y) {
        pixels -= image->width;
        if (fwrite(pixels, sizeof(pixel_t), image->width, out) != image->width) {
            return WRITE_ERROR;
        }
        fseek(out, padding, SEEK_CUR);
    }

    return WRITE_OK;
}
