#ifndef YASP_LAB5_BMP_PARSER_H
#define YASP_LAB5_BMP_PARSER_H

#include <stdio.h>
#include "../image.h"

typedef enum read_status_e {
    READ_OK = 0,
    READ_COULD_NOT_READ_HEADER,
    READ_INVALID_SIGNATURE,
    READ_UNSUPPORTED_HEADER_TYPE,
    READ_UNSUPPORTED_BITS_PER_PIXEL,
    READ_COULD_NOT_READ_PIXELS,
} read_status_t;

typedef enum write_status_e {
    WRITE_OK = 0,
    WRITE_ERROR,
} write_status_t;

read_status_t read_bmp(FILE * in, image_t * image);

write_status_t write_bmp(FILE * out, image_t * image);

#endif //YASP_LAB5_BMP_PARSER_H
