#include <malloc.h>
#include "image.h"

static inline uint32_t pixel_index(image_t const * image, uint32_t x, uint32_t y);

static inline uint32_t pixel_index(image_t const * const image, uint32_t x, uint32_t y)
{
    return y * image->width + x;
}

pixel_t * image_pixel(image_t const * const image, uint32_t x, uint32_t y)
{
    return image->pixels + pixel_index(image, x, y);
}

void image_free(image_t image)
{
    pixel_t * pixels = image.pixels;
    if (pixels != NULL) free(pixels);
    image.pixels = NULL;
}
