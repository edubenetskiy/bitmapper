#ifndef YASP_LAB5_IMAGE_H
#define YASP_LAB5_IMAGE_H

#include <stdint.h>
#include "pixel.h"

typedef struct image_s {
    uint32_t width;
    uint32_t height;
    pixel_t * pixels;
} image_t;

pixel_t * image_pixel(image_t const * image, uint32_t x, uint32_t y);

void image_free(image_t image);

#endif //YASP_LAB5_IMAGE_H
