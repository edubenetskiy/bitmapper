#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include "bmp/parser.h"
#include "transformations/rotate.h"
#include "transformations/gaussian_blur.h"
#include "transformations/dilate.h"
#include "transformations/erode.h"

static void display_usage(char const * program_name);

static image_t process(image_t const * image, const char * action);

int main(int argc, char * argv[])
{
    if (argc != 4) {
        if (argc > 1) {
            fputs("ERROR: Syntax error.\n", stderr);
        }
        display_usage(argv[0]);
        return ENOEXEC;
    }

    char * action = argv[1];
    char * source_filename = argv[2];
    char * target_filename = argv[3];

    FILE * source_file = fopen(source_filename, "rb");
    if (source_file == NULL) {
        fprintf(stderr, "ERROR: Could not open source file: %s\n", strerror(errno));
        return 1;
    }
    image_t source_image;
    read_status_t read_status = read_bmp(source_file, &source_image);
    switch (read_status) {
        case READ_COULD_NOT_READ_HEADER:
            fputs("ERROR: Could not read image header.\n", stderr);
            perror(NULL);
            break;
        case READ_INVALID_SIGNATURE:
            fputs("ERROR: Invalid format signature.\n", stderr);
            fputs("Maybe it is not a BMP file?\n", stderr);
            break;
        case READ_UNSUPPORTED_HEADER_TYPE:
            fputs("ERROR: Unsupported BMP header type.\n", stderr);
            fputs("Only 40-, 108-, and 124-bytes headers are supported.\n", stderr);
            break;
        case READ_UNSUPPORTED_BITS_PER_PIXEL:
            fputs("ERROR: Unsupported color depth.\n", stderr);
            fputs("Only 24 bits per pixel are supported.\n", stderr);
            fclose(source_file);
            return 1;
        case READ_COULD_NOT_READ_PIXELS:
            fputs("ERROR: Could not read pixel data.\n", stderr);
            break;
        case READ_OK:
            printf("Source image ‘%s’ loaded, size: %d×%d pixels.\n",
                   source_filename,
                   source_image.width,
                   source_image.height);
            break;
    }
    fclose(source_file);
    if (read_status != READ_OK) {
        return 1;
    }

    image_t target_image = process(&source_image, action);
    if (target_image.pixels == NULL) {
        fputs("ERROR: Invalid action.\n", stderr);
        display_usage(argv[0]);
        return 2;
    }
    image_free(source_image);

    FILE * target_file = fopen(target_filename, "wb");
    if (target_file == NULL) {
        fprintf(stderr, "ERROR: Could not open target file: %s\n", strerror(errno));
        return errno;
    }
    write_status_t write_status = write_bmp(target_file, &target_image);
    switch (write_status) {
        case WRITE_ERROR:
            fputs("ERROR: Could not write target image to file.\n", stderr);
            perror(NULL);
            break;
        case WRITE_OK:
            printf("Target image ‘%s’ written, size: %d×%d pixels.\n",
                   target_filename,
                   target_image.width,
                   target_image.height);
            break;
    }
    fclose(target_file);
    image_free(target_image);
    if (write_status != WRITE_OK) {
        return errno;
    }

    return 0;
};

void display_usage(char const * program_name)
{
    puts("USAGE:");
    printf("%s ACTION SOURCE_FILE.bmp TARGET_FILE.bmp", program_name);
    puts("");
    puts("Actions:");
    puts("    nop         perform no transformations");
    puts("    right       turn image right");
    puts("    left        turn image left");
    puts("    rotate+X    rotate image X degrees clockwise");
    puts("    rotate-X    rotate image X degrees anti-clockwise");
    puts("    blur        perform a Gaussian blur");
    puts("    dilate      perform a dilatation");
    puts("    erode       perform an erosion");
    puts("");
}

image_t process(image_t const * image, const char * action)
{
    if (strcmp(action, "nop") == 0) {
        return *image;
    } else if (strcmp(action, "right") == 0) {
        return rotate_right(image);
    } else if (strcmp(action, "left") == 0) {
        return rotate_left(image);
    } else if (strncmp(action, "rotate", strlen("rotate")) == 0) {
        const char * degree_string = action + strlen("rotate");
        double degrees = strtod(degree_string, NULL);
        if (errno == 0) return rotate_clockwise(image, degrees);
    } else if (strcmp(action, "dilate") == 0) {
        return dilate(image);
    } else if (strcmp(action, "erode") == 0) {
        return erode(image);
    } else if (strcmp(action, "blur") == 0) {
        return gaussian_blur(image);
    }

    // In case of errors, return dummy image.
    return (image_t) { width: 0, height:0, pixels: NULL };
}
