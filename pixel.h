#ifndef YASP_LAB5_PIXEL_H
#define YASP_LAB5_PIXEL_H

#include <stdint.h>

struct __attribute__((packed, aligned(1)))
pixel_s {
    uint8_t b, g, r;
};

typedef struct pixel_s pixel_t;

#endif //YASP_LAB5_PIXEL_H
