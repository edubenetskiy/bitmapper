#ifndef YASP_LAB5_POINT_H
#define YASP_LAB5_POINT_H

typedef struct point_s {
    double x, y;
} point_t;

#endif //YASP_LAB5_POINT_H
