#ifndef YASP_LAB5_DILATE_H
#define YASP_LAB5_DILATE_H

#include "../image.h"

image_t dilate(image_t const * source);

#endif //YASP_LAB5_DILATE_H
