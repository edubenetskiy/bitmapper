#include <malloc.h>
#include "../image.h"

pixel_t erode_pixel(image_t const * const image, uint32_t x, uint32_t y)
{
    if ((x == 0) || (x == image->width - 1)) return *image_pixel(image, x, y);
    if ((y == 0) || (y == image->height - 1)) return *image_pixel(image, x, y);

    pixel_t * pixels[9];

    pixels[0] = image_pixel(image, x - 1, y - 1);
    pixels[1] = image_pixel(image, x - 1, y);
    pixels[2] = image_pixel(image, x - 1, y + 1);

    pixels[3] = image_pixel(image, x, y - 1);
    pixels[4] = image_pixel(image, x, y);
    pixels[5] = image_pixel(image, x, y + 1);

    pixels[6] = image_pixel(image, x + 1, y - 1);
    pixels[7] = image_pixel(image, x + 1, y);
    pixels[8] = image_pixel(image, x + 1, y + 1);

    uint8_t r = UINT8_MAX,
            g = UINT8_MAX,
            b = UINT8_MAX;

    for (int i = 0; i < 9; ++i) {
        if (pixels[i]->r < r) r = pixels[i]->r;
        if (pixels[i]->g < g) g = pixels[i]->g;
        if (pixels[i]->b < b) b = pixels[i]->b;
    }

    pixel_t result = { r: r, g: g, b: b };
    return result;
}

image_t erode(image_t const * const source)
{
    image_t target = {
            width: source->width,
            height: source->height,
            pixels: calloc(source->width * source->height, sizeof(pixel_t)),
    };
    for (uint32_t x = 0; x < source->width; ++x) {
        for (uint32_t y = 0; y < source->height; ++y) {
            *image_pixel(&target, x, y) = erode_pixel(source, x, y);
        }
    }
    return target;
}
