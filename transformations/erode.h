#ifndef YASP_LAB5_ERODE_H
#define YASP_LAB5_ERODE_H

#include "../image.h"

image_t erode(image_t const * source);

#endif //YASP_LAB5_ERODE_H
