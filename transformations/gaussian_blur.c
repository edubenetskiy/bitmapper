#include <malloc.h>
#include "../image.h"

pixel_t average_pixel(image_t const * const image, uint32_t x, uint32_t y)
{
    if ((x == 0) || (x == image->width - 1)) return *image_pixel(image, x, y);
    if ((y == 0) || (y == image->height - 1)) return *image_pixel(image, x, y);

    pixel_t * pixels[9];
    pixels[0] = image_pixel(image, x - 1, y - 1);
    pixels[1] = image_pixel(image, x - 1, y);
    pixels[2] = image_pixel(image, x - 1, y + 1);

    pixels[3] = image_pixel(image, x, y - 1);
    pixels[4] = image_pixel(image, x, y);
    pixels[5] = image_pixel(image, x, y + 1);

    pixels[6] = image_pixel(image, x + 1, y - 1);
    pixels[7] = image_pixel(image, x + 1, y);
    pixels[8] = image_pixel(image, x + 1, y + 1);

    uint32_t r = 0, g = 0, b = 0;
    for (int i = 0; i < 9; ++i) {
        r += pixels[i]->r;
        g += pixels[i]->g;
        b += pixels[i]->b;
    }

    pixel_t average;
    average.r = (uint8_t) (r / 9);
    average.g = (uint8_t) (g / 9);
    average.b = (uint8_t) (b / 9);
    return average;
}

image_t gaussian_blur(image_t const * const source)
{
    image_t target = {
            width: source->width,
            height: source->height,
            pixels: calloc(source->width * source->height, sizeof(pixel_t)),
    };
    for (uint32_t x = 0; x < source->width; ++x) {
        for (uint32_t y = 0; y < source->height; ++y) {
            *image_pixel(&target, x, y) = average_pixel(source, x, y);
        }
    }
    return target;
}
