#ifndef YASP_LAB5_GAUSSIAN_BLUR_H
#define YASP_LAB5_GAUSSIAN_BLUR_H

#include "../image.h"

image_t gaussian_blur(image_t const * source);

#endif //YASP_LAB5_GAUSSIAN_BLUR_H
