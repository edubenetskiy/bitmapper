#include "rotate.h"
#include "../point.h"


static inline point_t rotate_point_clockwise(point_t point, double degrees)
{
    double radians = M_PI * degrees / 180;
    point_t target;
    target.x = point.x * cos(radians) - point.y * sin(radians);
    target.y = point.x * sin(radians) + point.y * cos(radians);
    return target;
}

static inline point_t rotate_point_anticlockwise(point_t point, double degrees)
{
    double radians = M_PI * degrees / 180;
    point_t target;
    target.x = point.y * sin(radians) + point.x * cos(radians);
    target.y = point.y * cos(radians) - point.x * sin(radians);
    return target;
}

image_t rotate_left(image_t const * const source)
{
    image_t target;
    target.height = source->width;
    target.width = source->height;
    uint32_t nmemb = target.height * target.width;
    target.pixels = calloc(nmemb, sizeof(pixel_t));

    for (uint32_t sx = 0, ty = target.height - 1; sx < source->width; ++sx, --ty) {
        for (uint32_t sy = 0, tx = 0; sy < source->height; ++sy, ++tx) {
            *image_pixel(&target, tx, ty) = *image_pixel(source, sx, sy);
        }
    }
    return target;
}

image_t rotate_right(image_t const * const source)
{
    image_t target;
    target.height = source->width;
    target.width = source->height;
    target.pixels = calloc(target.height * target.width, sizeof(pixel_t));
    for (uint32_t sx = 0, ty = 0; sx < source->width; ++sx, ++ty) {
        for (uint32_t sy = 0, tx = target.width - 1; sy < source->height; ++sy, --tx) {
            *image_pixel(&target, tx, ty) = *image_pixel(source, sx, sy);
        }
    }
    return target;
}

image_t rotate_clockwise(image_t const * const source, double degrees)
{
    // Source image geometry
    point_t sA = { x: 0.0, y: 0.0 };
    point_t sB = { x: source->width, y: 0.0 };
    point_t sC = { x: source->width, y: source->height };
    point_t sD = { x: 0.0, y: source->height };

    // Target image geometry
    point_t tA = rotate_point_clockwise(sA, degrees);
    point_t tB = rotate_point_clockwise(sB, degrees);
    point_t tC = rotate_point_clockwise(sC, degrees);
    point_t tD = rotate_point_clockwise(sD, degrees);

    double_t x_min = fmin(fmin(tA.x, tB.x), fmin(tC.x, tD.x));
    double_t y_min = fmin(fmin(tA.y, tB.y), fmin(tC.y, tD.y));

    double_t x_max = fmax(fmax(tA.x, tB.x), fmax(tC.x, tD.x));
    double_t y_max = fmax(fmax(tA.y, tB.y), fmax(tC.y, tD.y));

    image_t target = {
            width:  (uint32_t) ceil(x_max - x_min),
            height: (uint32_t) ceil(y_max - y_min),
    };

    target.pixels = calloc(target.width * target.height, sizeof(pixel_t));

    for (uint32_t tx = 0; tx < target.width; ++tx) {
        for (uint32_t ty = 0; ty < target.height; ++ty) {
            point_t tp = { x: tx + x_min, y: ty + y_min };
            point_t sp = rotate_point_anticlockwise(tp, degrees);

            if (sp.x < 0.0 || sp.y < 0.0) continue;
            uint32_t sx = (uint32_t) round(sp.x);
            uint32_t sy = (uint32_t) round(sp.y);
            if (sx >= source->width || sy >= source->height) continue;

            *image_pixel(&target, tx, ty) = *image_pixel(source, sx, sy);
        }
    }

    return target;
}
