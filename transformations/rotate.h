#ifndef YASP_LAB5_BMP_ROTATE_H
#define YASP_LAB5_BMP_ROTATE_H

#include <malloc.h>
#include <math.h>
#include "../image.h"

image_t rotate_left(image_t const * source);

image_t rotate_right(image_t const * source);

image_t rotate_clockwise(image_t const * source, double degrees);

#endif //YASP_LAB5_BMP_ROTATE_H
